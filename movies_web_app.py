import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    # table_ddl = 'CREATE TABLE message(id INT UNSIGNED NOT NULL AUTO_INCREMENT, greeting TEXT, PRIMARY KEY (id))'
    table_ddl = "CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, " \
        "director TEXT, actor TEXT, release_date TEXT, rating TEXT, PRIMARY KEY (id))"

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        # populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    db, username, password, hostname = get_db_creds()
    year = request.form.get('year',None)
    title = request.form.get('title',None)
    director = request.form.get('director',None)
    release_date = request.form.get('release_date',None)
    rating = request.form.get('rating',None)
    actor = request.form.get('actor',None)
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    data = "','".join([year,title,director,actor,release_date,rating])
    try:
        cur.execute("SELECT 1 FROM movies WHERE lower(title)='"+title.lower()+"'")
        if cur.fetchall():
            return render_template('index.html', message="Movie with "+title+" already exists")
        cur.execute("INSERT INTO movies (year,title,director,actor,release_date,rating) values ('" + data + "')")
        cnx.commit()
        result = "Movie "+title+" successfully inserted"
        return render_template('index.html', message=result)
    except Exception as exp:
        return render_template('index.html', message="Movie"+title+"could not be inserted - {}".format(exp))

@app.route('/update_movie', methods=['POST'])
def update_movie():
    db, username, password, hostname = get_db_creds()
    year = request.form.get('year',None)
    title = request.form.get('title',None)
    director = request.form.get('director',None)
    release_date = request.form.get('release_date',None)
    rating = request.form.get('rating',None)
    actor = request.form.get('actor',None)
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("SELECT 1 FROM movies WHERE lower(title)='"+title.lower()+"'")
        if not cur.fetchall():
            return render_template('index.html', message="Movie with "+title+" does not exist")
        cur.execute("UPDATE movies SET year='"+year+"', director='"+director+"', actor='"+actor+"', \
            release_date='"+release_date+ "', rating='"+rating+"' WHERE title='"+title+"'")
        cnx.commit()
        result = "Movie "+title+" successfully updated"
        return render_template('index.html', message=result)
    except Exception as exp:
        return render_template('index.html', message="Movie"+title+"could not be updated - {}".format(exp))

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    db, username, password, hostname = get_db_creds()
    title = request.form.get('delete_title',None)
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("SELECT 1 FROM movies WHERE lower(title)='"+title.lower()+"'")
        if not cur.fetchall():
            return render_template('index.html', message="Movie with "+title+" does not exist")
        cur.execute("DELETE FROM movies WHERE lower(title)='"+title.lower()+"'")
        cnx.commit()
        result = "Movie "+title+" successfully deleted"
        return render_template('index.html', message=result)
    except Exception as exp:
        return render_template('index.html', message="Movie"+title+"could not be deleted - {}".format(exp))


@app.route('/search_movie', methods=['GET','POST'])
def search_movie():
    db, username, password, hostname = get_db_creds()
    actor = request.form.get('search_actor',None)
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title,year,actor FROM movies where actor='"+actor+"'")
    rows = cur.fetchall()
    result = ""
    if rows:
        result = '<br>'.join(' '.join(x for x in row) for row in rows)
    else:
        result = "No movies found for actor " + actor
    return render_template('index.html', message=result)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title,year,actor,director,rating FROM movies WHERE rating=(SELECT MIN(rating) fROM movies)")
    rows = cur.fetchall()
    result = '<br>'.join(' '.join(x for x in row) for row in rows)
    return render_template('index.html', message=result)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title,year,actor,director,rating FROM movies WHERE rating=(SELECT MAX(rating) fROM movies)")
    rows = cur.fetchall()
    result = '<br>'.join(' '.join(x for x in row) for row in rows)
    return render_template('index.html', message=result)

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', message='Test message!')

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
